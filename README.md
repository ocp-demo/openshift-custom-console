# OpenShift Custom Console
<!-- TOC -->

- [OpenShift Custom Console](#openshift-custom-console)
  - [Custom Logo](#custom-logo)
  - [Custom Login Page](#custom-login-page)
  - [Custom Link](#custom-link)

<!-- /TOC -->
## Custom Logo
- Create configmap
```bash
oc create configmap console-custom-logo --from-file logo/custom-logo.png -n openshift-config
```
- Edit web console operator
```bash
oc edit console.operator.openshift.io cluster
```
- Sample YAML
```yaml
apiVersion: operator.openshift.io/v1
kind: Console
metadata:
  name: cluster
spec:
  customization:
    customLogoFile:
      key: custom-logo.png
      name: console-custom-logo
    customProductName: My Console
```
- Check operator status
```bash
oc get clusteroperator console -o yaml
```
Sample output while operate still processing
```yaml
conditions:
  - lastTransitionTime: "2020-10-10T05:30:29Z"
    reason: AsExpected
    status: "False"
    type: Degraded
  - lastTransitionTime: "2020-10-10T05:30:29Z"
    message: 'SyncLoopRefreshProgressing: Working toward version 4.5.7'
    reason: SyncLoopRefresh_InProgress
    status: "True"
    type: Progressing
```
- Check console
```bash
oc get console.operator.openshift.io -o yaml
```
- Sample screen
  
  ![Smudge lord](images/custom-logo-menu.png)

## Custom Login Page
- Sample template 
```bash
oc adm create-login-template > original-templates/login.html
oc adm create-provider-selection-template > original-templates/providers.html
oc adm create-error-template > original-templates/errors.html
```
- Sample custom login page [login.html](custom-login/login.html)
  - Only html stored in secret you need to host other required resources in other web server e.g. css,font,images etc
- Create secret to hold custom login page
```bash
oc create secret generic login-template --from-file=custom-login/index.html -n openshift-config
oc create secret generic providers-template --from-file=original-templates/providers.html -n openshift-config
oc create secret generic error-template --from-file=original-templates/errors.html -n openshift-config
```
- Update OAuth by run  *oc edit oauths cluster*
```yaml
spec:
  templates:
    error:
        name: error-template
    login:
        name: login-template
    providerSelection:
        name: providers-template
```
- Sample custom login page
  
  ![](images/custom-login-page.png)

## Custom Link
-  Create CRD [artifacts/custom-link.yaml](artifacts/custom-link.yaml)
  
```bash
oc apply -f artifacts/custom-link.yaml
```
- Location of custom menu
  - UserMenu
  - ApplicationMenu
  - NamespaceDashboard
  - HelpMenu 
- Sample custom menu

  ![](images/custom-menu.png)

  
